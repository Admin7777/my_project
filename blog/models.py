from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.auth.models import User

class Category(models.Model):
    title = models.CharField('Sarlavha', max_length=255)
    slug = models.SlugField('Silka', unique=True)
    image = models.ImageField('Rasm', blank=True, null=True)
    
    
    class Meta:
        verbose_name = 'Kategotiya'
        verbose_name_plural = 'Kategoriyalar'
    
    def get_absolute_url(self):
        return reverse("category_detail_url", kwargs={'slug':self.slug})

    def __str__(self):    
        return self.title__icontains
        
class Image(models.Model):
    image = models.ImageField('Picture', upload_to='images/')
    title = models.CharField('Title',max_length=255)

    class Meta:
        verbose_name = "picture"
        verbose_name_plural = "pictures"

    
    def __str__(self):
        return self.title

class Post(models.Model):
    author = models.ForeignKey(User,on_delete=models.CASCADE, verbose_name = "Author", null=True)
    title = models.CharField("Sarlovha", max_length=255)
    slug = models.SlugField("Silka", unique=True)
    image = models.ImageField("Rasm", blank=True, null=True)
    video = models.FileField('Video', upload_to='videos/', null=True)
    content = models.TextField("Text")
    date = models.DateTimeField('Sana', default=timezone.now)
    publish = models.BooleanField("Taqdim etish", default=False)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Category', related_name='posts')

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Postlar'
        
    def __str__(self):
       return self.title
       
    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug':self.slug})

    def __str__(self):    
        return self.title

class View(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)



class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Avtor")
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name="Yangilik")
    text = models.TextField("Kommentariya", null=True)
    data = models.DateTimeField("Sana", default=timezone.now)


    class Meta:
        verbose_name = "Coment"
        verbose_name_plural ="Coments"

    def __str__(self):   
        return self.user.username        
 
