from django.contrib import admin
from .models import Category, Post, Comment, Image 



class SlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('title',)}

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']

class ArticleAdmin(admin.ModelAdmin):
    list_display = ['category', ...]

admin.site.register(Category, SlugAdmin)
admin.site.register(Post, SlugAdmin)
admin.site.register(Comment)
admin.site.register(Image)
